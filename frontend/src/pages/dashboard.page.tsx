import React, { FC, useState, useEffect, useCallback } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import { Button } from "@material-ui/core";

const backendClient = new BackendClient();

let count = 0;

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const [cards, setCards] = useState<Array<JSX.Element>>([]);
  const [card, setCard] = useState<Array<JSX.Element>>([]);

  const [generator, setGenerator] = useState<Generator<JSX.Element>>();

  const callbackGeneratorCards = useCallback(
    function* generatorCards(): Generator<JSX.Element> {
      let index = 0;

      while (index < users.length) {
        let user = users[index];
        index++;

        // fix key
        yield <UserCard key={user.id + index} {...user} />;
      }
    },
    [users]
  );

  const callNextCard = useCallback((): void => {
    if (!generator) return;

    let card = generator.next().value;

    if (!card) return;

    setCards((cards) => [...cards, card]);
    setCard(card);
  }, [generator, setCards, setCard]);

  const renderCards = (): Array<JSX.Element> => {
    return users.map((user) => {
      console.log("RENDER BUNCH OF USERS")
      return <UserCard key={user.id} {...user} />;
    });
  };

  useEffect(() => {
    console.log("SET GENERATOR");
    setGenerator(callbackGeneratorCards());
  }, [callbackGeneratorCards]);

  useEffect(() => {
    console.log("FETCH USER DATA");
    const fetchData = async () => {
      const result = await backendClient.getAllUsers();
      setUsers(result.data.splice(0, 100));
      setLoading(false);
    };

    fetchData();
  }, [setUsers, setLoading]);

  // // run every cards change, until the .next finished entire array
  // useEffect(() => {
  //   console.log("UPDATE CARDS");
  //   console.log("COUNT UPDATES: ", count++);
  //   setTimeout(() => {
  //     callNextCard();
  //   });
  // }, [callNextCard, cards]);

  return (
    <div style={{ paddingTop: "30px" }}>
      <Button
        style={{
          display: "block",
          margin: "0 auto",
        }}
        variant="contained"
        onClick={(): void => {
          alert("Async");
        }}
      >
        Test Async
      </Button>
      <div>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <>
            <div>{card ? card : null}</div>
            <div>{cards.length ? cards : null}</div>
            {renderCards()}
          </>
        )}
      </div>
    </div>
  );
};
